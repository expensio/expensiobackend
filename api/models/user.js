const mongoose = require('mongoose');

const userSchema = {
    _id: mongoose.Schema.Types.ObjectId,
    username: {type: String, required: true},
    userpassword: {type: String, required: true},
    useremail: {type: String, required: true},
    userphone: {type: String, required: false}
}

module.exports = mongoose.model('User', userSchema);