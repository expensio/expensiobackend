const express = require('express');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const router = express.Router();

const User = require('../models/user.js')

// POST requests in /users/signup
router.post('/signup', (req, res, next) => {
    console.log(req.body);
    bcrypt.hash(req.body.userpassword, 10, (err, hash) => {
        if(err) {
            return res.status(500).json({
                error: err
            });
        } 
        else {
            const user = new User({
                _id: new mongoose.Types.ObjectId,
                username: req.body.username,
                userpassword: hash,
                useremail: req.body.useremail,
                userphone: req.body.userphone
            });
            user.save()
            .then(result => {
                res.status(201).json({
                    message: 'User created.'
                });
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                    message: 'Error creating user.'
                });
            });
        }
    });
});

module.exports = router;