const express = require('express');
const expressBodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');
const secrets = require('./secrets');

// routes
const userRoutes = require('./api/routes/user_router.js');

// credentials
const mongoCredentials = 'mongodb+srv://' + secrets.mongouser + ':' + secrets.mongopassword + '@cluster0-2ewgf.mongodb.net/test?retryWrites=true&w=majority';

mongoose.connect(
    mongoCredentials,
    { useNewUrlParser: true }
);

app.use(expressBodyParser.json());

// Routes that handle requests
app.use('/users', userRoutes);

app.get('/', (req, res) => {
    res.send("we are on home");
})

app.listen(3000);